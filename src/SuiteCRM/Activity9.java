package SuiteCRM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity9 {
	private WebDriver driver;
	private WebDriverWait wait;
	Actions builder;
	
	
  @BeforeClass
public void intiliaze() {
	  
	  driver = new FirefoxDriver();
      wait = new WebDriverWait(driver,20);
      builder = new Actions(driver);
      
      driver.get("https://alchemy.hguy.co/crm");
		
  }
  
  @AfterClass
  public void cleanUp() {
	  
	  driver.close();
  }
  
  @Test
  public void login() {
	  
		//Get the username text box
		WebElement username = driver.findElement(By.id("user_name"));
		  

		  //Get the password text box
		  WebElement password = driver.findElement(By.id("username_password"));
		  
		  //Get the Login button
		  WebElement loginButton = driver.findElement(By.id("bigbutton"));
		  
		  username.sendKeys("admin");
		  
		  password.sendKeys("pa$$w0rd");
		  
		  loginButton.click();
		  
		  //Wait for the new page to open
		  wait = new WebDriverWait(driver,20);
		  
		  		  
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#with-label > span:nth-child(2)")));
		  wait.until(ExpectedConditions.titleIs("SuiteCRM"));
		  
		   
  
  }
  @Test (dependsOnMethods = {"login"})
  public void selectLeadVerify() throws InterruptedException {
  Actions builder = new Actions(driver);
  
  WebElement salesMenu = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
	//salesMenu.click();
	
	Action hoverOverActivity = builder.click(salesMenu).build();
	hoverOverActivity.perform();
	
	//Select Leads SubMenu		
	WebElement leadsMenu = driver.findElement(By.cssSelector("a[id='moduleTab_9_Leads']"));
	
	
	//Select the Leads Menu
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click()", leadsMenu);
	
	//leadsMenu.click();

  
  
	wait.until(ExpectedConditions.titleContains("Leads"));
	
	
	//WebElement iButton = driver.findElement();
	//System.out.println("The class of the iButton is"+iButton.getClass());
	
	
	Thread.sleep(2000);

	/*List<WebElement> nameList = driver.findElements(By.xpath("//tr[@class='oddListRowS1']//td[@field='name']/b/a"));
	
	for(WebElement name:nameList) {
		
		System.out.println("The name in the list is" +name.getText());
	}*/
	
  //Get the Names in the table and print it in the Console	
  List<WebElement> nameList = driver.findElements(By.xpath("//td[@field='name']/b/a"));
	
	for(WebElement name:nameList) {
		
		System.out.println("The name in the list is" +name.getText());
	}
	
   List<WebElement> userList = driver.findElements(By.xpath("//td[@field='assigned_user_name']/a"));
	
	for(WebElement user:userList) {
		
		System.out.println("The User in the list is  " +user.getText());
	}
	
    List<WebElement> accList = driver.findElements(By.xpath("//td[@field='account_name']"));
	
	for(WebElement user:accList) {
		
		System.out.println("The Account in the list is  " +user.getText());
	}
	
  }
  

  
}
