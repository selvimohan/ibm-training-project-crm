package SuiteCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class LoginPage {
	
  private WebDriver driver;
  //private WebDriver wait;
  
  @BeforeClass
  public void openBrowser() {
	  
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm");
	  
  }
  
  @AfterClass
  public void closeBrowser() {
	  driver.close();
  }
  
  @Test
  @Parameters ({"pUserName","pPassword"})
  public void login(String pUserName, String pPassword) {
	  //Get the username text box
	  WebElement username = driver.findElement(By.id("user_name"));
	  

	  //Get the password text box
	  WebElement password = driver.findElement(By.id("username_password"));
	  
	  //Get the Login button
	  WebElement loginButton = driver.findElement(By.id("bigbutton"));
	  
	  username.sendKeys(pUserName);
	  
	  password.sendKeys(pPassword);
	  
	  loginButton.click();
	 	  
  }


}
