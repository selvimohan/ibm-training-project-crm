package SuiteCRM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity7 {
	

	private WebDriver driver;
	private WebDriverWait wait;
	private Actions builder;
	
	  @BeforeClass
	  public void openBrowser() {
		  
		  driver = new FirefoxDriver();
		  driver.get("https://alchemy.hguy.co/crm");
		  
		  
		  
	  }
	  
	  @AfterClass
	  public void closeBrowser() {
		  driver.close();
	  }
	  
	  @Test (alwaysRun=true)	  
	  public void login() throws Throwable{
		  //Get the username text box
		  WebElement username = driver.findElement(By.id("user_name"));
		  

		  //Get the password text box
		  WebElement password = driver.findElement(By.id("username_password"));
		  
		  //Get the Login button
		  WebElement loginButton = driver.findElement(By.id("bigbutton"));
		  
		  username.sendKeys("admin");
		  
		  password.sendKeys("pa$$w0rd");
		  
		  loginButton.click();
		  
		  //Wait for the new page to open
		  wait = new WebDriverWait(driver,20);
		  
		  		  
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#with-label > span:nth-child(2)")));
		  wait.until(ExpectedConditions.titleIs("SuiteCRM"));
		  
		  Thread.sleep(2000);
		  
	}

	@Test (dependsOnMethods = {"login"})
	public void clickLeadsMenu() throws InterruptedException {
		
		builder = new Actions(driver);
		
		//Get the  SalesMenu
		WebElement salesMenu = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
		
		//Click the Sales Menu
		Action hoverOverActivity = builder.click(salesMenu).build();
		hoverOverActivity.perform();
		
		//Select Leads SubMenu		
		WebElement leadsMenu = driver.findElement(By.cssSelector("a[id='moduleTab_9_Leads']"));
		
		
		//Select the Leads Menu
		leadsMenu.click();
		
}
	    
  @Test (dependsOnMethods = {"clickLeadsMenu"})
  public void clickAdditionInfo () throws InterruptedException {
	  
	  Thread.sleep(2000);

	  //Select the first "i" Symbol
	  WebElement iSymbol = driver.findElement(By.xpath("//span[contains(@class,'suitepicon-action-info')]"));	
		
			//Click the "i" Symbol
			iSymbol.click();
			
			Thread.sleep(2000);
			
			//Get the content 
			
			//WebElement mobile = driver.findElement(By.xpath("//div[starts-with(@aria-describedby, 'ui-id-')]//span[@class='phone']"));
			
			//Get the Phone Field 
			WebElement mobile = driver.findElement(By.xpath("//div[starts-with(@id, 'ui-id-')]//span[@class='phone']"));
			
			//Get the phone text
			String mobileText = mobile.getText();
			
			System.out.println("The Mobile number of the first entry is" +mobileText);
			
			
			
			//Close the dialog box
			WebElement closeButton = driver.findElement(By.xpath("//div[starts-with(@aria-describedby, 'ui-id-')]//span[@class='ui-button-text' and text()='Close']"));
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
			
			js.executeScript("arguments[0].click()", closeButton);
			
		
  

  }
  

}
