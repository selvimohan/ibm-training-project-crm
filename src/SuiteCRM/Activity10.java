package SuiteCRM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity10 {
	
	private WebDriver driver;
    private WebDriverWait wait;
    Actions builder;


@BeforeClass
public void intiliaze() {
  
  driver = new FirefoxDriver();
  wait = new WebDriverWait(driver,20);
  builder = new Actions(driver);
  
  driver.get("https://alchemy.hguy.co/crm");
	
}

@AfterClass
public void cleanUp() {
  
  driver.close();
}

@Test
public void login() throws InterruptedException {
	  
		//Get the username text box
		WebElement username = driver.findElement(By.id("user_name"));
		  

		  //Get the password text box
		  WebElement password = driver.findElement(By.id("username_password"));
		  
		  //Get the Login button
		  WebElement loginButton = driver.findElement(By.id("bigbutton"));
		  
		  username.sendKeys("admin");
		  
		  password.sendKeys("pa$$w0rd");
		  
		  loginButton.click();
		  
		  //Wait for the new page to open
		  WebElement tool = driver.findElement(By.id("toolbar"));
		  wait.until(ExpectedConditions.visibilityOf(tool));
		  
		  wait.until(ExpectedConditions.titleIs("SuiteCRM"));
		  
		  Thread.sleep(2000);
		   

}
@Test (dependsOnMethods = {"login"})
public void countDashlets() {
	
	//Get the count of the dashlets in the page
	  
	  //List<WebElement> num = driver.findElements(By.xpath("//ul/li[contains(@id,'dashlet')]//span"));
	  
	  /*List<WebElement> num = driver.findElements(By.xpath("//div[contains(@id,'dashlet_header')]"));
			 
	  System.out.println("The num is" +num.size());
	  */
	  
	  //Thread.sleep(2000);
	  
	  
	//  List<WebElement> lisL = driver.findElements(By.xpath("//table[contains(@class,'dashletTable')]//ul/li[contains(@id,'dashlet_')]"));
	  
	  //System.out.println("The Total Number of Dashlets in the page is" +lisL.size());

	  //String reg ="//td[contains(@class,'dashletcontainer')]//td[contains(@class,'dashlet-title')]/h3/span[contains(@class,'suitepicon-module-')]";
	  
	  
	  
	    
	  //List<WebElement> nameList = driver.findElements(By.xpath("//td[contains(@class,'dashletcontainer')]//td[contains(@class,'dashlet-title')]/h3/span[contains(@class,'suitepicon-module-')]"));
	  
	  List<WebElement> nameList = driver.findElements(By.xpath("//td[contains(@class,'dashletcontainer')]//td[contains(@class,'dashlet-title')]/h3/span[2]"));
	  
	  
	  System.out.println("The Total Number of Dashlet is  "+nameList.size());
	  
	 for (WebElement name:nameList) {
					 
		System.out.println("The Name of the Dashboard is  " +name.getText());
	 }
	  
	
}

}
