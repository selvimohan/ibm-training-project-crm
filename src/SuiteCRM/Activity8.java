package SuiteCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity8 {
	
	private WebDriver driver;
	private WebDriverWait wait;
	Actions builder;
	
  @BeforeClass
  public void intiliaze() {
	  
	  driver = new FirefoxDriver();
      wait = new WebDriverWait(driver,20);
      builder = new Actions(driver);
      
      driver.get("https://alchemy.hguy.co/crm");
		
		
  }
  
  @AfterClass
  public void cleanUp() {
	  
	  driver.close();
  }
  
  @Test
  public void login() {
	  
		//Get the username text box
		WebElement username = driver.findElement(By.id("user_name"));
		  

		  //Get the password text box
		  WebElement password = driver.findElement(By.id("username_password"));
		  
		  //Get the Login button
		  WebElement loginButton = driver.findElement(By.id("bigbutton"));
		  
		  username.sendKeys("admin");
		  
		  password.sendKeys("pa$$w0rd");
		  
		  loginButton.click();
		  
		  //Wait for the new page to open
		  wait = new WebDriverWait(driver,20);
		  
		  		  
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#with-label > span:nth-child(2)")));
		  wait.until(ExpectedConditions.titleIs("SuiteCRM"));
		  
		   
  
  }
  @Test (dependsOnMethods = {"login"})
  public void selectAccMenu() throws InterruptedException {
	  
	  WebElement salesMenu = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
		
	  Action hoverOverActivity = builder.click(salesMenu).build();
		hoverOverActivity.perform();
		
		//Select Accounts SubMenu		
		WebElement accMenu = driver.findElement(By.cssSelector("a[id='moduleTab_9_Accounts']"));
		
		
		//Select the Leads Menu
		accMenu.click();
		
		wait.until(ExpectedConditions.titleContains("Accounts"));
		
		
		//WebElement iButton = driver.findElement();
		//System.out.println("The class of the iButton is"+iButton.getClass());
		
		
		Thread.sleep(2000);

		for (int i = 1;i<=9;i+=2) {
		
			
			WebElement firstName = driver.findElement(By.xpath("/html/body/div[4]/div/div[3]/form[2]/div[3]/table/tbody/tr["+i+"]/td[3]/b/a"));
			
			System.out.println("tr[" +i+ "]td[3] is   " +firstName.getText());
			System.out.println("The" +i+ "st Account is    " +firstName.getText());
			
		}

	  
	  
  }
}
