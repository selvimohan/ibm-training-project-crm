package SuiteCRM;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity11 {
	
	private WebDriver driver;
    private WebDriverWait wait;
    
    @BeforeClass
    public void intiliaze() {
      
      driver = new FirefoxDriver();
      wait = new WebDriverWait(driver,20);
      
      driver.get("https://alchemy.hguy.co/crm");
    	
    }

    @AfterClass
    public void cleanUp() {
      
      driver.close();
    }

    @Test
    public void login() throws InterruptedException {
    	  
    		//Get the username text box
    		WebElement username = driver.findElement(By.id("user_name"));
    		  

    		  //Get the password text box
    		  WebElement password = driver.findElement(By.id("username_password"));
    		  
    		  //Get the Login button
    		  WebElement loginButton = driver.findElement(By.id("bigbutton"));
    		  
    		  username.sendKeys("admin");
    		  
    		  password.sendKeys("pa$$w0rd");
    		  
    		  loginButton.click();
    		  
    		  //Wait for the new page to open
    		  WebElement tool = driver.findElement(By.id("toolbar"));
    		  wait.until(ExpectedConditions.visibilityOf(tool));
    		  
    		  wait.until(ExpectedConditions.titleIs("SuiteCRM"));
    		  
    		  Thread.sleep(2000);
    		   

    }

    
    
  @Test(dependsOnMethods = {"login"})
  public void importLeads() throws InterruptedException {
	  
	   
	  Actions builder = new Actions(driver);
	  
	  WebElement salesMenu = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
		
		Action hoverOverActivity = builder.click(salesMenu).build();
		hoverOverActivity.perform();
		
		Thread.sleep(2000);
		
		WebElement leadsMenu = driver.findElement(By.xpath("//a[@id='moduleTab_9_Leads']"));
		
		
		//Select the Leads Menu
		leadsMenu.click();
		
		
		//WebElement iButton = driver.findElement();
		//System.out.println("The class of the iButton is"+iButton.getClass());
		
		Thread.sleep(2000);

    	//Select Import Leads Menu
		
		WebElement importLeadMenu = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='Import Leads']"));
		
		
		importLeadMenu.click();
		
	    WebElement fileSelect = driver.findElement(By.id("userfile"));	
		
		wait.until(ExpectedConditions.visibilityOf(fileSelect));
		try {
			File file = new File("src/SuiteCRM/Leads.csv");
			WebElement fileSelect1 = driver.findElement(By.xpath("//input[@type='file']"));
			
			fileSelect1.sendKeys(file.getAbsolutePath());
				
			
			
			WebElement nextButton = driver.findElement(By.xpath("//input[@id='gonext' and @class='button']"));
			nextButton.click();
			
			System.out.println("Next button is clicked");
			Thread.sleep(4000);
			
			driver.findElement(By.id("gonext")).click();
			
			System.out.println("Second Next button is clicked");
			
			Thread.sleep(4000);
			
            WebElement gonext = driver.findElement(By.id("gonext"));
            
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].click();", gonext);
			
			System.out.println("Third Next button is clicked");
			
			Thread.sleep(4000);
			
			WebElement import1 = driver.findElement(By.id("importnow"));
			js.executeScript("arguments[0].click();", import1);
			
			System.out.println("Selected the import now button");
			
			Thread.sleep(4000);
			
			String text = driver.findElement(By.className("module-title-text")).getText();
			System.out.println("Text is" +text);
			
			Thread.sleep(2000);
			
			Assert.assertEquals("STEP 5: VIEW IMPORT RESULTS", text);
			
			WebElement result = driver.findElement(By.xpath("//div[@class='screen']/span"));
			String result1 = result.getText();
			if(result1.contains("records were created")) {
				System.out.println("Import Successfull" +result1);
					
			} else {
				
				System.out.println("Import Unsuccessfull" +result1);
				
			}
			
			
			Thread.sleep(4000);
		}
		catch (Exception e) {
			
			System.out.println(e.getStackTrace());
		}
		
		
WebElement viewLeadMenu = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='View Leads']"));
		
Thread.sleep(2000);
		
		viewLeadMenu.click();
		
		System.out.println("View lead  is clicked");
		
		Thread.sleep(2000);
		      

  }
}
