package SuiteCRM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity12 {

	private WebDriver driver;
	private WebDriverWait wait;
	
	
  @BeforeClass
  public void before() {
	  
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm");
	  
  
  }
  
  
  @AfterClass
  public void after() {
	  
	  driver.close();
  }
  
  
  @Test (alwaysRun=true)	  
  public void login() throws Throwable{
	  //Get the username text box
	  WebElement username = driver.findElement(By.id("user_name"));
	  

	  //Get the password text box
	  WebElement password = driver.findElement(By.id("username_password"));
	  
	  //Get the Login button
	  WebElement loginButton = driver.findElement(By.id("bigbutton"));
	  
	  username.sendKeys("admin");
	  
	  password.sendKeys("pa$$w0rd");
	  
	  loginButton.click();
	  
	  //Wait for the new page to open
	  wait = new WebDriverWait(driver,20);
	  
	  		  
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#with-label > span:nth-child(2)")));
	  wait.until(ExpectedConditions.titleIs("SuiteCRM"));
	  
	  //Thread.sleep(2000);
	  
}

  @Test (dependsOnMethods= {"login"})
  public void clickActivityMenu() throws InterruptedException {
	  
	  JavascriptExecutor js = (JavascriptExecutor) driver;
	  
	  //Locate Activity Menu
	  WebElement activityMenu = driver.findElement(By.id("grouptab_3"));
	  
	  js.executeScript("arguments[0].click()", activityMenu);
	  
	  WebElement meetingMenu = driver.findElement(By.id("moduleTab_9_Meetings"));
	  
	  js.executeScript("arguments[0].click()", meetingMenu);
	  
	  wait.until(ExpectedConditions.titleContains("Meetings"));
	  
	  Thread.sleep(4000);
	  
	  //Select Schedule a meeting
	  WebElement scheduleMeet = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='Schedule Meeting']"));
	  
	  js.executeScript("arguments[0].click()", scheduleMeet);
	  
	  Thread.sleep(2000);
	  
	  //System.out.println("The title is" +driver.getTitle());
	  
	 wait.until(ExpectedConditions.titleContains("CREATE � Meetings"));
	 
  }
  
  @Test(dependsOnMethods= {"clickActivityMenu"})
  public void scheduleMeet() throws InterruptedException {
	  
	  //Send value for the subject
	  driver.findElement(By.id("name")).sendKeys("Testing");
	  
		 
	  
	  WebElement startDatePick = driver.findElement(By.xpath("//button[@id='date_start_trigger']"));
	  
	  startDatePick.click();
	  
	  WebElement selectMonth = driver.findElement(By.xpath("//th[@class='calhead']//a[@class='calnav']"));
	  
	  selectMonth.click();
	  
	  WebElement monthList = driver.findElement(By.id("date_start_trigger_div_nav_month"));
	  
	  Select monthPick = new Select(monthList);
	  
	  monthPick.selectByVisibleText("Feb");
	  
	  //Thread.sleep(2000);
	  
	  //Submit the month
	  
	  driver.findElement(By.id("date_start_trigger_div_nav_submit")).click();
	  
	//Select the date
	  WebElement dateSelect = driver.findElement(By.xpath("//table[@id='date_start_trigger_div_t']//tbody[contains(@class,'calbody')]//a[text()='10' and @class='selector']"));
	  
	  dateSelect.click();
	  
	  //Select the Hours
	  WebElement selectHrs = driver.findElement(By.id("date_start_hours"));
	  
	  Select selHrs = new Select(selectHrs);
	  
	  selHrs.selectByVisibleText("09");
	  
	 //Select the Minutes
	  WebElement selectMin = driver.findElement(By.id("date_start_minutes"));
	  
	  Select selMin = new Select(selectMin);
	  
	  selMin.selectByVisibleText("45");
	  
	  //Select the duration
	  WebElement duration = driver.findElement(By.id("duration"));
	  
	  Select durationSet = new Select(duration);
	  
	  durationSet.selectByIndex(2);
	  
	  
	  //Select the reminder
	  driver.findElement(By.id("reminder_add_btn")).click();
	  
	  //Thread.sleep(2000);
	  
	  
	  //Check the checkbox in the reminder popup and email
	  driver.findElement(By.xpath("//li[contains(@class,'reminder_item')]//input[@class='popup_chkbox']")).click();
	  
	  driver.findElement(By.xpath("//li[contains(@class,'reminder_item')]//input[@class='email_chkbox']")).click();
	  
	  //Thread.sleep(2000);
	  
	  
	  
	  //Select the prior time
	  WebElement reminderTime = driver.findElement(By.xpath("//li[contains(@class,'reminder_item')]//select[@class='timer_sel_popup']"));
			  
			
	  Select remindT = new Select(reminderTime);
	  
	  remindT.selectByIndex(3);
	  
	  //Select all the invitess button
	  
	  driver.findElement(By.xpath("//li[contains(@class,'reminder_item')]//button[contains(@class,'add-btn')]")).click();
	  
	  //Thread.sleep(4000);
	  
	  //JavascriptExecutor js = (JavascriptExecutor) driver;
	  
	 // js.executeScript("window.scrollBy(0,300)");
	  
	  
	  
	  //Type the description
	  driver.findElement(By.xpath("//textarea[@id='description']")).sendKeys("Testing Meeting");
	  
	  //Thread.sleep(4000);
	  
	  //Search first name
	  driver.findElement(By.id("search_first_name")).sendKeys("QA");
	  
	  //js.executeScript("window.scrollBy(0,1000)");
	  
	  //Thread.sleep(4000);
	  
	  
	  //Select Search button
	  driver.findElement(By.id("invitees_search")).click();
	  
	 // js.executeScript("window.scrollBy(0,1500)");
	  
	 
	  Thread.sleep(2000);
	  
	   
	  List<WebElement> element1 = driver.findElements(By.xpath("//div[@id='list_div_win']/table[contains(@class,'list view')]/tbody/tr")); 
	  
	  System.out.println("Element 1 is" + element1.size());
	  
	  List<WebElement> element2 = driver.findElements(By.xpath("//input[contains(@id,'invitees_add_')]"));

	  System.out.println("Element 2 is" + element2.size());
	 
	  //List<String> button = new ArrayList<>();

	  for (int i = 1;i<=element1.size()-1;i++) {
	  
		  
		  System.out.println("The value is" + element1.get(i).getText());
		  
		  //WebElement arr = driver.findElement(By.xpath("//input[contains(@id,'invitees_add_')]"));
		  
		  System.out.println("Element 2 Attribue ID  first value is i"+ i+ "-1" +element2.get(i-1).getAttribute("id"));
		  
		   element2.get(i-1).click(); 
		  
		  
		  Thread.sleep(2000);
		  
		  
		  
	  }
	  
	  
	  //Select the type of invitee
	  //js.executeScript("window.scrollBy(0,2000)");
	  
	  //driver.findElement(By.xpath("//div[@id='create-invitees']//div[@id='create-invitees-buttons']/button[contains(@id,'_as_contact')]")).click();
	  
	  //Select Save
	  driver.findElement(By.id("save_and_send_invites_header")).click();
	  
	   
	  Thread.sleep(4000);
	  
	  
  }
  
  @Test (dependsOnMethods= {"scheduleMeet"})
  public void viewMeetings() throws InterruptedException {
	  
	  WebElement viewMeetingMenu = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='View Meetings']"));
	  
	 
	  
	  viewMeetingMenu.click();
	  Thread.sleep(2000);
	  //System.out.println("Page title is"+driver.getTitle());
	  
	  wait.until(ExpectedConditions.titleContains("Meetings �"));
	  
	  //Verify if the meeting is created
	 // String str = "Testing";
	  
	  //WebElement text = driver.findElement(By.xpath("//td[@class='inlineEdit']/b/a[text()='"+str+"']"));
	  
	  //String text1 = text.getText();
 //List<WebElement> element1 = driver.findElements(By.xpath("//div[@id='list_div_win']/table[contains(@class,'list view')]/tbody/tr//a[text()='Testing']")); 
	 
	  //Click the created Schedule meeting
	  driver.findElement(By.linkText("Testing")).click();
	  
	  
	  Thread.sleep(4000);
		
	
	  
  }



}
