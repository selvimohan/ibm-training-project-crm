package SuiteCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity5  {

	private WebDriver driver;
	private WebDriverWait wait;
	
	
	  @BeforeClass
	  public void openBrowser() {
		  
		  driver = new FirefoxDriver();
		  driver.get("https://alchemy.hguy.co/crm");
		  
	  }
	  
	  @AfterClass
	  public void closeBrowser() {
		  driver.close();
	  }
	  
	  @Test (alwaysRun=true)	  
	  public void login() throws Throwable{
		  //Get the username text box
		  WebElement username = driver.findElement(By.id("user_name"));
		  

		  //Get the password text box
		  WebElement password = driver.findElement(By.id("username_password"));
		  
		  //Get the Login button
		  WebElement loginButton = driver.findElement(By.id("bigbutton"));
		  
		  username.sendKeys("admin");
		  
		  password.sendKeys("pa$$w0rd");
		  
		  loginButton.click();
		  
		  //Wait for the new page to open
		  wait = new WebDriverWait(driver,20);
		  
		 // wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='tab0']")));	  		  
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#with-label > span:nth-child(2)")));
		  wait.until(ExpectedConditions.titleIs("SuiteCRM"));
		  
		  Thread.sleep(2000);
		  
	}

	@Test (dependsOnMethods = {"login"})
	public void getColorNavigation() {
		
		//Get the Navigation Bar
		WebElement navigationBar = driver.findElement(By.cssSelector("ul[id='toolbar']"));
				
		
		
		System.out.println("The Color of the Navigation Menu is" + navigationBar.getCssValue("color"));
		
		
	
 }

}
