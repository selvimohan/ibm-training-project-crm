package SuiteCRM;

import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity13 {

	private WebDriver driver;
	private WebDriverWait wait;
	
	
  @BeforeClass
  public void before() {
	  
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm");
	  
  
  }
  
  
  @AfterClass
  public void after() {
	  
	  driver.close();
  }
  
  
  @Test (alwaysRun=true)	  
  public void login() throws Throwable{
	  //Get the username text box
	  WebElement username = driver.findElement(By.id("user_name"));
	  

	  //Get the password text box
	  WebElement password = driver.findElement(By.id("username_password"));
	  
	  //Get the Login button
	  WebElement loginButton = driver.findElement(By.id("bigbutton"));
	  
	  username.sendKeys("admin");
	  
	  password.sendKeys("pa$$w0rd");
	  
	  loginButton.click();
	  
	  //Wait for the new page to open
	  wait = new WebDriverWait(driver,20);
	  
	  		  
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#with-label > span:nth-child(2)")));
	  wait.until(ExpectedConditions.titleIs("SuiteCRM"));
	  
	  //Thread.sleep(2000);
	  
}

  @Test (dependsOnMethods= {"login"})
  public void createProduct() throws InterruptedException, IOException {
	  
	  JavascriptExecutor js = (JavascriptExecutor) driver;
		Actions builder = new Actions(driver);
		
	//Select All Menu
	  WebElement allMenu = driver.findElement(By.xpath("//a[@id='grouptab_5']"));
	  
	  builder.click(allMenu).build().perform();
	  
	  Thread.sleep(2000);
	 
	  WebElement dropDown = allMenu.findElement(By.xpath("//ul[@class='dropdown-menu']/li[25]/a"));
	  
	  js.executeScript("arguments[0].scrollIntoView(true);", dropDown);
	  
	  //System.out.println("The drop Down value is"+dropDown.getText());
	  
	  js.executeScript("arguments[0].click();", dropDown);
	  
	  
	  //Thread.sleep(2000);
	  
	  wait.until(ExpectedConditions.titleContains("Products"));
	  
	  Thread.sleep(2000);
	  
	  WebElement createProd = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='Create Product']"));
	  
	  createProd.click();
	  
	  Thread.sleep(2000);
	  
	  wait.until(ExpectedConditions.titleContains("Products �"));

	  //Read the file 
	  String filePath = "src/SuiteCRM/ProductFile2.xlsx";
	  
	  FileInputStream file = new FileInputStream(filePath);
	  
	  XSSFWorkbook workbook = new XSSFWorkbook(file);
	  
	  XSSFSheet sheet = workbook.getSheetAt(0);
	  
	  //Store the cell values in an array
	  
	  System.out.println("The total no of rows is" + sheet.getPhysicalNumberOfRows());
	  
	  int rowNum = sheet.getPhysicalNumberOfRows()-1;
	  //int colNum = sheet.getRow(0).getLastCellNum();
	  int colNum = sheet.getRow(0).getPhysicalNumberOfCells();
	  
	  System.out.println("The Row Size is"+rowNum);
	  System.out.println("The Column Size is"+colNum);
	  
	  String[][] data = new String[rowNum][colNum];
	  for (int i = 0;i<=rowNum-1;i++) {
		  
		  XSSFRow row = sheet.getRow(i);
		  //System.out.println("The Row value is"+sheet.getRow(i));
		    
		  //  int w = row.getLastCellNum()-1;
		   // int w1 = row.getPhysicalNumberOfCells();
		    
		   // System.out.println( "Row's Last Cell num - 1 is " +w);
		    //System.out.println( "Row's Physical No of Cells  is " +w1);
		    
		 //   for(int j=0;j<=row.getLastCellNum();j++) {
		    for(int j=0;j<colNum;j++) {
		    	
		    	XSSFCell cell = row.getCell(j);
		    	if(cell==null) {
		    		data[i][j] = "";
			       
		    	} else {
		    		String k = row.getCell(j).toString();
			       	data[i][j] = k;
			       
		    	}
		    	//System.out.println("Cell value is"+row.getCell(j));
		    	
		    	
		    	
		        //System.out.println("data[" +i+ "]["+ j+"] is" + data[i][j]);
		    	//DataFormatter dataFormat = new DataFormatter();
		    	//dataFormat.formatCellValue(cell);
		    	//data[i][j] = cell.getStringCellValue();
		   
		    	//data[i][j] = dataFormat.formatCellValue(cell);
		    	
		    	
		    	
		    }
		  
	  }
	    
	  System.out.println("The size of the array is" + data.length);
	  
	  
	  for(int l=1;l<=data.length-1;l++) {
	
		  
		  //Locate the Product Name
		  WebElement productName = driver.findElement(By.xpath("//input[@id='name']"));
		 // System.out.println("Data of colum 0 is "+data[l][0]); 
		  productName.sendKeys(data[l][0]);
		  
		  //Locate Cost
		  WebElement cost = driver.findElement(By.xpath("//input[@id='cost']"));
		  //System.out.println("Data of colum 1 is "+data[l][1]);
		  cost.sendKeys(data[l][1]);
		  
		  //Locate Contact
		  WebElement contact = driver.findElement(By.xpath("//input[@id='contact']"));
		  //System.out.println("Data of colum 2 is "+data[l][2]);
		  contact.sendKeys(data[l][2]);
		  
		  //Locate Description
		  WebElement desc = driver.findElement(By.xpath("//textarea[@id='description']"));
		  //System.out.println("Data of colum 3 is "+data[l][3]);
		  desc.sendKeys(data[l][3]);
		  
		  //Locate PartNo
		  WebElement partNo = driver.findElement(By.xpath("//input[@id='part_number']"));
		  //System.out.println("Data of colum 4 is "+data[l][4]);
		  partNo.sendKeys(data[l][4]);
		  
		  //Locate Product Type
		  WebElement productTypeList = driver.findElement(By.xpath("//select[@id='type']"));
		  Select productType = new Select(productTypeList);
		  
		  //System.out.println("The options size is"+productType.getOptions().size());
		  
		 // System.out.println("The options value 0 is"+productType.getOptions().get(0).getText());
		  
		 //System.out.println("The options value 1 is"+productType.getOptions().get(1).getText());
		  System.out.println("Data 1 5 is value is"+data[l][5]);
		  
		 int no = data[l][5].compareTo("Service");
		 if(no == 0) {
		 
			 productType.selectByVisibleText("Service");
			  //System.out.println("The selected value is" +productType.getFirstSelectedOption().getText());
		  } 
		 
		  System.out.println("The selected value is" +productType.getFirstSelectedOption().getText());
		  
		  //Locate Price
		  WebElement price = driver.findElement(By.xpath("//input[@id='price']"));
		  //System.out.println("Data of colum 6 is "+data[l][6]);
		  price.sendKeys(data[l][6]);
		  
		  //Locate url
		  WebElement url = driver.findElement(By.xpath("//input[@id='url']"));
		  //System.out.println("Data of colum 7 is "+data[l][7]);
		  url.sendKeys(data[l][7]);
		  
		  //Select Save Button
		  WebElement saveButton = driver.findElement(By.xpath("//input[@id='SAVE' and @title='Save']"));
		  saveButton.click();
		  
		 Thread.sleep(4000);
		
		//Select View Product
		 WebElement viewProd = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='View Products']"));
		  
		// JavascriptExecutor js = (JavascriptExecutor) driver;
		 js.executeScript("arguments[0].click()", viewProd);
		 
		 
		//Verify the product
		 //System.out.println(driver.findElement(By.linkText(data[l][0])).getText());
		/* if (driver.findElement(By.linkText(data[l][0])).isDisplayed()) {
			 
			 System.out.println ("The Product" + data[l][0]+"is created successfully");
			 
		 } else {
			 System.out.println ("The Product" + data[l][0]+"is not created ");
		 }*/
		 
		 
		 
		 Thread.sleep(4000);
	  
				 
		 //Select Create Product
		 WebElement createProd1 = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='Create Product']"));
		  
		// JavascriptExecutor js = (JavascriptExecutor) driver;
		 js.executeScript("arguments[0].click()", createProd1);
		 
		 //createProd1.click();
		 
		 Thread.sleep(4000);
			
		
		  
	  }
	  
	  
		 Thread.sleep(2000);
		 
		 
	  file.close();
	  
	  workbook.close();
	  
	  
	  
	  
	
	   
  }
  
  


}
